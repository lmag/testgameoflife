﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class Test : System.Web.UI.Page
    {
        bool[,] SquareStatus = new bool[12, 12];

        bool[,] RefreshStatus = new bool[12, 12];

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            for (int row = 0; row < SquareStatus.GetLength(0) - 1; row++)
            {
                for (int col = 0; col < SquareStatus.GetLength(1) - 1; col++)
                {
                    SquareStatus[row, col] = false;
                }
            }

            Button1.Click += (Button1, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 1, Button1); };
            Button2.Click += (Button2, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 2, Button2); };
            Button3.Click += (Button3, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 3, Button3); };
            Button4.Click += (Button4, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 4, Button4); };
            Button5.Click += (Button5, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 5, Button5); };
            Button6.Click += (Button6, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 6, Button6); };
            Button7.Click += (Button7, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 7, Button7); };
            Button8.Click += (Button8, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 8, Button8); };
            Button9.Click += (Button9, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 9, Button9); };
            Button10.Click += (Button10, EventArgs) => { buttonNext_Click(sender, EventArgs, 1, 10, Button10); };

            Button11.Click += (Button11, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 1, Button11); };
            Button12.Click += (Button12, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 2, Button12); };
            Button13.Click += (Button13, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 3, Button13); };
            Button14.Click += (Button14, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 4, Button14); };
            Button15.Click += (Button15, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 5, Button15); };
            Button16.Click += (Button16, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 6, Button16); };
            Button17.Click += (Button17, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 7, Button17); };
            Button18.Click += (Button18, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 8, Button18); };
            Button19.Click += (Button19, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 9, Button19); };
            Button20.Click += (Button20, EventArgs) => { buttonNext_Click(sender, EventArgs, 2, 10, Button20); };

            Button21.Click += (Button21, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 1, Button21); };
            Button22.Click += (Button22, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 2, Button22); };
            Button23.Click += (Button23, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 3, Button23); };
            Button24.Click += (Button24, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 4, Button24); };
            Button25.Click += (Button25, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 5, Button25); };
            Button26.Click += (Button26, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 6, Button26); };
            Button27.Click += (Button27, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 7, Button27); };
            Button28.Click += (Button28, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 8, Button28); };
            Button29.Click += (Button29, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 9, Button29); };
            Button30.Click += (Button30, EventArgs) => { buttonNext_Click(sender, EventArgs, 3, 10, Button30); };

            Button31.Click += (Button31, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 1, Button31); };
            Button32.Click += (Button32, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 2, Button32); };
            Button33.Click += (Button33, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 3, Button33); };
            Button34.Click += (Button34, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 4, Button34); };
            Button35.Click += (Button35, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 5, Button35); };
            Button36.Click += (Button36, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 6, Button36); };
            Button37.Click += (Button37, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 7, Button37); };
            Button38.Click += (Button38, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 8, Button38); };
            Button39.Click += (Button39, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 9, Button39); };
            Button40.Click += (Button40, EventArgs) => { buttonNext_Click(sender, EventArgs, 4, 10, Button40); };

            Button41.Click += (Button41, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 1, Button41); };
            Button42.Click += (Button42, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 2, Button42); };
            Button43.Click += (Button43, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 3, Button43); };
            Button44.Click += (Button44, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 4, Button44); };
            Button45.Click += (Button45, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 5, Button45); };
            Button46.Click += (Button46, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 6, Button46); };
            Button47.Click += (Button47, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 7, Button47); };
            Button48.Click += (Button48, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 8, Button48); };
            Button49.Click += (Button49, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 9, Button49); };
            Button50.Click += (Button50, EventArgs) => { buttonNext_Click(sender, EventArgs, 5, 10, Button50); };

            Button51.Click += (Button51, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 1, Button51); };
            Button52.Click += (Button52, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 2, Button52); };
            Button53.Click += (Button53, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 3, Button53); };
            Button54.Click += (Button54, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 4, Button54); };
            Button55.Click += (Button55, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 5, Button55); };
            Button56.Click += (Button56, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 6, Button56); };
            Button57.Click += (Button57, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 7, Button57); };
            Button58.Click += (Button58, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 8, Button58); };
            Button59.Click += (Button59, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 9, Button59); };
            Button60.Click += (Button60, EventArgs) => { buttonNext_Click(sender, EventArgs, 6, 10, Button60); };

            Button61.Click += (Button61, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 1, Button61); };
            Button62.Click += (Button62, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 2, Button62); };
            Button63.Click += (Button63, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 3, Button63); };
            Button64.Click += (Button64, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 4, Button64); };
            Button65.Click += (Button65, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 5, Button65); };
            Button66.Click += (Button66, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 6, Button66); };
            Button67.Click += (Button67, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 7, Button67); };
            Button68.Click += (Button68, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 8, Button68); };
            Button69.Click += (Button69, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 9, Button69); };
            Button70.Click += (Button70, EventArgs) => { buttonNext_Click(sender, EventArgs, 7, 10, Button70); };

            Button71.Click += (Button71, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 1, Button71); };
            Button72.Click += (Button72, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 2, Button72); };
            Button73.Click += (Button73, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 3, Button73); };
            Button74.Click += (Button74, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 4, Button74); };
            Button75.Click += (Button75, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 5, Button75); };
            Button76.Click += (Button76, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 6, Button76); };
            Button77.Click += (Button77, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 7, Button77); };
            Button78.Click += (Button78, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 8, Button78); };
            Button79.Click += (Button79, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 9, Button79); };
            Button80.Click += (Button80, EventArgs) => { buttonNext_Click(sender, EventArgs, 8, 10, Button80); };

            Button81.Click += (Button81, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 1, Button81); };
            Button82.Click += (Button82, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 2, Button82); };
            Button83.Click += (Button83, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 3, Button83); };
            Button84.Click += (Button84, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 4, Button84); };
            Button85.Click += (Button85, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 5, Button85); };
            Button86.Click += (Button86, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 6, Button86); };
            Button87.Click += (Button87, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 7, Button87); };
            Button88.Click += (Button88, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 8, Button88); };
            Button89.Click += (Button89, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 9, Button89); };
            Button90.Click += (Button90, EventArgs) => { buttonNext_Click(sender, EventArgs, 9, 10, Button90); };

            Button91.Click += (Button91, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 1, Button91); };
            Button92.Click += (Button92, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 2, Button92); };
            Button93.Click += (Button93, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 3, Button93); };
            Button94.Click += (Button94, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 4, Button94); };
            Button95.Click += (Button95, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 5, Button95); };
            Button96.Click += (Button96, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 6, Button96); };
            Button97.Click += (Button97, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 7, Button97); };
            Button98.Click += (Button98, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 8, Button98); };
            Button99.Click += (Button99, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 9, Button99); };
            Button100.Click += (Button100, EventArgs) => { buttonNext_Click(sender, EventArgs, 10, 10, Button100); };

        }
        #endregion

        protected void buttonNext_Click(object sender, EventArgs e, int row, int column, object Button)
        {
            var button = Button as Button;
            button.CssClass = "buttongreen";
            SquareStatus[row, column] = true;
        }

        protected void RefreshView()
        {
            for (int row = 0; row < RefreshStatus.GetLength(0) - 1; row++)
            {
                for (int col = 0; col < RefreshStatus.GetLength(1) - 1; col++)
                {
                    RefreshStatus[row, col] = false;
                }
            }

            int alive = 0;
            for (int row = 1; row < SquareStatus.GetLength(0) - 2; row++)
            {
                alive = 0;
                for (int col = 1; col < SquareStatus.GetLength(1) - 2; col++)
                {
                    //bottom left
                    if (SquareStatus[row - 1, col - 1])
                    {
                        alive++;
                    }

                    //top left
                    if (SquareStatus[row - 1, col + 1])
                    {
                        alive++;
                    }

                    //top right
                    if (SquareStatus[row + 1, col + 1])
                    {
                        alive++;
                    }

                    //bottom right
                    if (SquareStatus[row + 1, col - 1])
                    {
                        alive++;
                    }

                    //bottom
                    if (SquareStatus[row, col - 1])
                    {
                        alive++;
                    }

                    //top
                    if (SquareStatus[row, col + 1])
                    {
                        alive++;
                    }

                    //left
                    if (SquareStatus[row - 1, col])
                    {
                        alive++;
                    }

                    //right
                    if (SquareStatus[row + 1, col])
                    {
                        alive++;
                    }

                    if (alive == 0 || alive == 1)
                    {
                        RefreshStatus[row, col] = false;
                    }

                    if (alive >= 4)
                    {
                        RefreshStatus[row, col] = false;
                    }

                    if (alive == 2 || alive == 3)
                    {
                        RefreshStatus[row, col] = true;
                    }
                }
            }
        }

        protected void StartButton_Click(object sender, EventArgs e)
        {
            Timer2.Enabled = true;
            StartButton.Enabled = false;
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            RefreshView();
            for (int row = 0; row < RefreshStatus.GetLength(0) - 1; row++)
            {
                for (int col = 0; col < RefreshStatus.GetLength(1) - 1; col++)
                {
                    SquareStatus[row, col] = RefreshStatus[row, col];

                    var ButtonNumber = (row - 1) * 10 + col;
                    var ButtonID = "Button" + ButtonNumber;
                    var control = this.FindControl(ButtonID);
                    var button = control as Button;
                    if (button != null)
                    {
                        if (SquareStatus[row, col])
                        {
                            button.CssClass = "buttongreen";
                        }
                        else
                        {
                            button.CssClass = "button";
                        }
                    }

                }
            }
        }
    }
}